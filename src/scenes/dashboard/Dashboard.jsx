import React from 'react';

import { DashboardContainer } from '../../modules/dashboard';
import Maintemplate from '../../shared/templates/MainTemplate/MainTemplateContainer';

const Dashboard = () => (
  <Maintemplate>
    <DashboardContainer />
  </Maintemplate>
);

export default Dashboard;
